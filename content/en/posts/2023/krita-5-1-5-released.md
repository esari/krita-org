---
title: "Krita 5.1.5 Released"
date: "2023-01-05"
categories: 
  - "officialrelease"
---

We're releasing today a new bugfix release. This is mainly important for Android and ChromeOS users where sometimes Krita would be very slow opening or creating a new image, or loading an image.

## Fixes

- Fixed an issue with banding when using the gradient map filter as a filter layer. [BUG:463585](https://bugs.kde.org/show_bug.cgi?id=463585)
- Only center paste into active layer if contents are outside image bounds. [BUG:461894](https://bugs.kde.org/show_bug.cgi?id=461894)
- EXR: Fixed opening EXR files that have only a luma channel. [BUG:461975](https://bugs.kde.org/show_bug.cgi?id=461975)
- Fixed an issue building with XSIMD 10.0 and up. [BUG:463219](https://bugs.kde.org/show_bug.cgi?id=463219)
- Properly use the resolution of a .KRA file when that file is used as a file layer. [BUG:442127](https://bugs.kde.org/show_bug.cgi?id=442127)
- Fixed a regression in loading PSD files. [BUG:462417](https://bugs.kde.org/show_bug.cgi?id=462417)
- Android: Fixed loading and creating images on Android and ChromeOS. [BUG:463318](https://bugs.kde.org/show_bug.cgi?id=463318)
- Android: Fixed an issue with loading translations. [BUG:462943](https://bugs.kde.org/show_bug.cgi?id=462943)

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.1.5-setup.exe](https://download.kde.org/stable/krita/5.1.5/krita-x64-5.1.5-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.1.5.zip](https://download.kde.org/stable/krita/5.1.5/krita-x64-5.1.5.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.1.5/krita-x64-5.1.5-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.1.5-x86\_64.appimage](https://download.kde.org/stable/krita/5.1.5/krita-5.1.5-x86_64.appimage)

The separate gmic-qt appimage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.1.5.dmg](https://download.kde.org/stable/krita/5.1.5/krita-5.1.5.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface needs a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.1.5/krita-x86_64-5.1.5-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/stable/krita/5.1.5/krita-x86-5.1.5-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.1.5/krita-arm64-v8a-5.1.5-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.1.5/krita-armeabi-v7a-5.1.5-release-signed.apk)

### Source code

- [krita-5.1.5.tar.gz](https://download.kde.org/stable/krita/5.1.5/krita-5.1.5.tar.gz)
- [krita-5.1.5.tar.xz](https://download.kde.org/stable/krita/5.1.5/krita-5.1.5.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.1.5/](https://download.kde.org/stable/krita/5.1.5) and click on Details to get the hashes.

### Key

The Linux appimage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/4DA79EDA231C852B). The signatures are [here](https://download.kde.org/stable/krita/5.1.5/) (filenames ending in .sig).
