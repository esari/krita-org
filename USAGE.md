# Day-to-day Usage

This document contains instructions on how to do non-developer type things like creating news posts, uploading images, or doing a release. You can do all of these tasks if you bring down the code base with GIT and follow the readme, but you can also use an embeded code editor in the browser to make certain things a bit easier. We will go over how to do things inthe online code editor with this page, but the process is almost identical if you choose to pull the code down with GIT to make changes.

## Accessing the online code editor

To do changes to krita.org, you will need to create an account on [invent.kde.org](https://invent.kde.org). There are a lot of projects on this site, so the first step is to find krita.org.

![](static/readme/tutorial-1.png)

![](static/readme/tutorial-2.png)

Once you are on the project, you will find an edit button where you can open the code editor (called Web IDE) to make changes. 

![](static/readme/tutorial-3.png)

_Note: You need to be an admin for the krita.org project to be able to push to the live krita.org site._

## Creating a news post

Now that we found our way into the online code editor, it is time to create a new post. This code editor is a branch of Microsoft's VSCode if you are familiar with that. The screenshot below shows that you can see all the files in the project by clicking on the icon circled in red below. This tab is usually selected by default when you load the code editor.

![](static/readme/tutorial-4.png)

News posts are organided in the content folder. The __en__ language is important as that is the default language all other languages will use when translations are done. When you have the "2024" folder selected, click on the + icon circled in the screenshot below.



![](static/readme/tutorial-5.png)

A new document is created. Enter a file name for the news post. This name is what appears in the URL, so make sure to not use things like space characters. All news posts end with .md, meaning it is a markdown file. It will be blank, so you can look at another news post to see the general format.

Let's reference another post in 2024 to see how. Double click the other .md file in the 2024 folder to open it in the code editor.

## Understanding the metadata at the top of a post

![](static/readme/tutorial-7.png)

The top element of the post in-between three hyphens is called the front matter. This is the metadata associated with the news posts. The types of data you put here are the following:

- title - What will appear at the top of the page
- date - The release date of the post.
- draft - Do not publish the post when you finish. It won't exist at all on the site. ( e.g. draft: "true" ) 
- hidden - The post will exist when you publish, but will not appear in the news.  ( e.g. hidden: "true" ) 

The "hidden" metadata is good for doing work in progress posts that you want to share with others. The final link will be public, so you can share it out without the post actually being on the news area. Hidden posts are also searchable, so it will be easier to find than trying to figure out what the URL is.

## Making changes to your news post and saving

![](static/readme/tutorial-7.png)

Fill in your news post with some new information. In this example we are not creating a "hidden" metadata to true, but that will probably be more common. After you do your changes, you can save it next. 

Whenever you make changes to the code, you will see a purple icon appear on the right side by the fourth icon circled in red. Click the icon that appears circled in red below. 

![](static/readme/tutorial-9.png)

This shows a history of all the things that have changed. When you save changes, you will need to make a message with what you did. In this example you can see I wrote that I created a new post. To save, click the "Commit to master" button.

You will get a dialog below like this. You can click "Continue". The main reason this appears is that when you save it, a new build will kick off for the site and it will be updated since it is using the master branch.

![](static/readme/tutorial-10.png)

On the bottom right, there is a message that appears that takes you back to the project.

![](static/readme/tutorial-11.png)


Back to the project view, you will see your new message as the first item...meaning it was the most recently saved/committed.

![](static/readme/tutorial-12.png)

When you save, it actually kicks off a new build. To see the progress go to the left area "Build" > "Pipelines" and click that. When the build is done it will show a green checkmark.

![](static/readme/tutorial-13.png)

Congratulations, you have successfully made a new post. If you visit the site, your changes will be live.

## Uploading Images

You can upload image files to the static folder. Everything in the static folder will be published when the site builds. It is a good idea to try to keep things organized in folders. You can manually upload files in the web editor through the following right click option circled in red below.

![](static/readme/tutorial-14.png)

You can add image files by using this format in markdown. There is a [good cheat sheet](https://www.markdownguide.org/cheat-sheet/) if you want to see various markdown formats for adding images, or other files. You can also just reference other posts on this site to how things are done.


## New releases

In addition to doing a news post for a new release, you will need to update a lot of the links and version numbers on the download page. All of this data can be changed in the __data > releases.yaml__ file. You can update versions and all the URLs for the binary files at this location.

If for some reason you need to update more things on the download page that aren't referenced in that file, see the location __layouts > \_default > download.html__ to make manual changes.

## Artist Interview posts

Two extra settings need to be added to the front matter (top metdata) part of the post to make it an artist interview. Reference another artist interview on the site to be sure, but these sections should be added.
    
    categories:
        - artist-interview
    coverImage: "Girl-Redraw.webp"

The coverImage value tries to be a bit clever. The image looks in the __static > images > posts > {year}__ folder with the associated year of the publication date. As long as the file name is correct the image should appear.